$PARENT_FOLDER="./"
$outputFile="BuildAllImages-out.txt"
$errorFIle="BuildAllImages-error.txt"
Write-Host "" > $outputFile
Write-Host "" > $errorFIle
$Languages_Folders = Get-ChildItem -Path $PARENT_FOLDER | Where-Object { $_.PSIsContainer -eq $true }
foreach($folderLanguage in $Languages_Folders){
    "$($folderLanguage)" >> $outputFile
    "$($folderLanguage)" >> $errorFIle
    docker build --tag "alpine-$($folderLanguage.Name.ToLower())" $folderLanguage\. 1>> $outputFile 2>> $errorFIle
}



