#!/bin/bash
ruby /develop/main.rb < /develop/execution/input.txt 1> /develop/execution/execution-output.txt 2>/develop/execution/execution-error.txt
echo $? > /develop/execution/execution-exitcode.txt
exit $?