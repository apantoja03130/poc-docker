# poc-docker
**First** run:

`.\BuildAllImages.ps1`

That builds all Dockerfiles, and outputs messages are located at: BuildAllImages-out.txt, BuildAllImages-error.txt



**Second** if you want to see the images generated (optional), you should run:

`.\GetImagesList.ps1`

Generate the list of images succesfuly builded, the list are generated at: ImageList.txt



**Third**, to compile and/or run a program you need to execute:

`.\RunImage.ps1 language folderLocation`


Where: 

    -language can be: cpp,java,ruby,python

    -folderLocation: is the fullpath of shared folder between containers and the server


Sample:

`.\RunImage.ps1 cpp d:\develop`

But before you need to create the folderLocation with two subfolders(compilation and execution), 
Sample:
d:\develop
    - \compilation
    - \ejecution
and add your code in that folder with a particular name:
main.cpp
or
Main.java
or
main.py
or
main.rb
(there is a sample folder in the develop-folder branch)

